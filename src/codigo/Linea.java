/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author joseignacionavassanz
 */
public class Linea extends Polygon{
    
    public Color miColor = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    int x = 0;
    int y = 0;

    //Constructor de la clase
    public Linea (int _x, int _y, int _width, Color _miColor) {

        npoints = 2;

        this.x = _x;
        this.y = _y;

        this.xpoints[0] = _x;
        this.ypoints[0] = _y;

        this.xpoints[1] = _x + _width*4;
        this.ypoints[1] = _y;
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;

    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAncho, int nuevoAlto) {
        
        this.xpoints[0] = x;
        this.ypoints[0] = y;

        this.xpoints[1] = x + nuevoAncho;
        this.ypoints[1] = y + nuevoAlto;
    }
}