/*
 * Mi tercera clase JAVA

 La clase Cuadrado permite almacenar un cuadrado
 además de su posición en la pantalla. También
 permite saber si está coloreado o no y en
 que color está coloreado en caso de estarlo
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author Jose Ignacio Navas Sanz
 */
public class Cuadrado extends Polygon {
    
    //Variable de color public, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color miColor = null;
    
    //Variable booleana que determinará el relleno de la figura
    //Inicializada a false y public para llamarla en VentanaDibujo
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    public int x = 0;
    public int y = 0;

    //Constructor de la clase
    public Cuadrado(int _x, int _y, int _width, Color _miColor, boolean _relleno) {
        
        //Definimos el número de puntos del cuadrado
        this.npoints = 4;
        
        /*
        Definimos los puntos X e Y
        Mediante this podemos apuntar 
        a las variables de instancia
        y decirles el nuevo valor que
        van a tomar.
         */
        this.x = _x;
        this.y = _y;
        
        //Definimos los puntos del cuadrado
        /*
           Punto 2 ________ Punto 1
                  |        |
                  |        |
                  |        |
           Punto 3|________|Punto 0
        */
        
        //Punto 0
        this.xpoints[0] = _x + (_width / 2);
        this.ypoints[0] = _y - (_width / 2);
        //Punto 1
        this.xpoints[1] = _x + (_width / 2);
        this.ypoints[1] = _y + (_width / 2);
        //Punto 2
        this.xpoints[2] = _x - (_width / 2);
        this.ypoints[2] = _y + (_width / 2);
        //Punto 3
        this.xpoints[3] = _x - (_width / 2);
        this.ypoints[3] = _y - (_width / 2);
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;
    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAncho) {
        /*
        Puntos para la redimension del Cuadrado
        Simplemente es copiar los puntos definidos
        previamente y cambiar _width por nuevoAncho
        */
        
        //Punto 0
        this.xpoints[0] = x + (nuevoAncho / 2);
        this.ypoints[0] = y - (nuevoAncho / 2);
        
        //Punto 1
        this.xpoints[1] = x + (nuevoAncho / 2);
        this.ypoints[1] = y + (nuevoAncho / 2);

        //Punto 2
        this.xpoints[2] = x - (nuevoAncho / 2);
        this.ypoints[2] = y + (nuevoAncho / 2);

        //Punto 3
        this.xpoints[3] = x - (nuevoAncho / 2);
        this.ypoints[3] = y - (nuevoAncho / 2);
    }

}
