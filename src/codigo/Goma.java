package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Jose Ignacio Navas Sanz
 * 
 * La clase Círculo pintará círculos
 * en la clase principal del programa
 * VentanaDibujo.
 */
public class Goma extends Ellipse2D.Double {
    
    //Variable de color public, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color miColor = Color.WHITE;
    
    //Variable booleana que determinará el relleno de la figura
    //Inicializada a false y public para llamarla en VentanaDibujo
    public boolean relleno = false;
    
    //Constructor de la clase
    public Goma(int _x, int _y, int _width, Color _miColor, boolean _relleno) {
        
        //Apuntamos a los nuevos valores de las variables
        this.x = _x - _width / 2;
        this.y = _y - _width / 2;
        //Nuevo valor del ancho y del alto
        this.width = _width;
        this.height = _width;
        //Establecemos el valor del color y del relleno
        this.miColor = _miColor;
        this.relleno = _relleno;

    }

    //Método pintaYColorea
    /*
    Establece el color de la figura
    así cómo el color que tendrá
    su relleno en caso de tenerlo
    */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

}
