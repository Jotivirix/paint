/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author joseignacionavassanz
 */
public class Hexagon extends Polygon {
    
    //Variable de color publica, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color miColor = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    int x = 0;
    int y = 0;

    //Constructor de la clase
    public Hexagon(int _x, int _y, int _width, int _height, Color _miColor, boolean _relleno) {
                
        this.x = _x;
        this.y = _y;
        
        /*
        Para que la dimensión de la cruz no
        sea excesivamente grande, establecemos
        el ancho a la mitad del definido
        */
        int ancho = _width;
        int alto = _height;
        
        //Añadimos los puntos que componen la Cruz
        //Punto 0
        addPoint(_x - ancho/4, _y - alto/2);
        //Punto 1
        addPoint(_x - ancho/2, _y);
        //Punto 2
        addPoint(_x - ancho/4, _y + alto/2);
        //Punto 3
        addPoint(_x + ancho/4, _y + alto/2);
        //Punto 4
        addPoint(_x + ancho/2, _y);
        //Punto 5
        addPoint(_x + ancho/4 , _y - alto/2);
        
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;
    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAncho, int nuevoAlto) {
              
        this.xpoints[0] = x - nuevoAncho/4;
        this.ypoints[0] = y - nuevoAlto/2;
               
        this.xpoints[1] = x - nuevoAncho/2;
        this.ypoints[1] = y;
        
        this.xpoints[2] = x - nuevoAncho/4;
        this.ypoints[2] = y + nuevoAlto/2;
        
        this.xpoints[3] = x + nuevoAncho/4;
        this.ypoints[3] = y + nuevoAlto/2;
      
        this.xpoints[4] = x + nuevoAncho/2;
        this.ypoints[4] = y;
        
        this.xpoints[5] = x + nuevoAncho/4;
        this.ypoints[5] = y - nuevoAlto/2;    
    }
}