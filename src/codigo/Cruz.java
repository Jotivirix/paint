/*
 * Mi quinta clase JAVA

 La clase Cruz permite almacenar una cruz de 12
 puntos, además de su posición en la pantalla.
 Permite saber si está coloreado o no y de que
 color lo está

 LOS PUNTOS DE MI CRUZ SON:
        2________3
         |      |
         |      |
 0______1|______|4_____5
 |       |      |      |        EL ANCHO ES LA MITAD DEL RADIO  
 |       |      |      |        DE CADA CADRUADO
 |_______|______|______|        ____________________
 11    10|      |7      6      |                    |
         |      |              |                    |
        9|______|8             |                    |
                               |                    |
                               |---------RADIO      |
                               |                    |
                               |                    |
                               |                    |
                               |____________________|

*/                            
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author Jose Ignacio Navas Sanz
 */
public class Cruz extends Polygon {
    
    //Variable de color public, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color miColor = null;
    
    //Variable booleana que determinará el relleno de la figura
    //Inicializada a false y public para llamarla en VentanaDibujo
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    int x = 0;
    int y = 0;

    //Constructor de la clase
    public Cruz(int _x, int _y, int _width, Color _miColor, boolean _relleno) {
        
        this.x = _x;
        this.y = _y;
        
        /*
        Para que la dimensión de la cruz no
        sea excesivamente grande, establecemos
        el ancho a la mitad del definido
        */
        int ancho = _width / 6;
        
        //Añadimos los puntos que componen la Cruz
        
        //Punto 0
        addPoint(_x - ((3*ancho)), _y + ancho);
        
        //Punto 1
        addPoint(_x - ancho, _y + ancho);
        
        //Punto 2
        addPoint(_x - ancho, _y + ((3*ancho)));
        
        //Punto 3
        addPoint(_x + ancho, _y + ((3*ancho)));
        
        //Punto 4
        addPoint(_x + ancho, _y + ancho);
        
        //Punto 5
        addPoint(_x + ((3*ancho)), _y + ancho);
            
        //Punto 6
        addPoint(_x + ((3*ancho)), _y - ancho);
        
        //Punto 7
        addPoint(_x + ancho, _y - ancho);
        
        //Punto 8
        addPoint(_x + ancho, _y - ((3*ancho)));
        
        //Punto 9
        addPoint(_x - ancho, _y - ((3*ancho)));
        
        //Punto 10
        addPoint(_x - ancho, _y - ancho);
        
        //Punto 11
        addPoint(_x - ((3*ancho)), _y - ancho);
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;
    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAncho, int nuevoAlto) {
        
        nuevoAncho = nuevoAncho/6;
        nuevoAlto = nuevoAlto/6;
        
        /*
        Definimos los puntos para la redimensión
        Simplemente es copiar y pegar los puntos
        definidos anteriormente en la CRUZ y cambiar
        el ancho por nuevoAncho y el alto por nuevoAlto
        */
       
        //Punto 0
        this.xpoints[0] = x - 3*nuevoAncho;
        this.ypoints[0] = y + nuevoAlto;
        
        //Punto 1
        this.xpoints[1] = x - nuevoAncho;
        this.ypoints[1] = y + nuevoAlto;
          
        //Punto 2
        this.xpoints[2] = x - nuevoAncho;
        this.ypoints[2] = y + 3*nuevoAlto;
        
        //Punto 3
        this.xpoints[3] = x + nuevoAncho;
        this.ypoints[3] = y + 3*nuevoAlto;
       
        //Punto 4
        this.xpoints[4] = x + nuevoAncho;
        this.ypoints[4] = y + nuevoAlto;
        
        //Punto 5
        this.xpoints[5] = x + 3*nuevoAncho;
        this.ypoints[5] = y + nuevoAlto;
        
        //Punto 6
        this.xpoints[6] = x + 3*nuevoAncho;
        this.ypoints[6] = y - nuevoAlto;
        
        //Punto 7
        this.xpoints[7] = x + nuevoAncho;
        this.ypoints[7] = y - nuevoAlto;
        
        //Punto 8
        this.xpoints[8] = x + nuevoAncho;
        this.ypoints[8] = y - 3*nuevoAlto;
        
        //Punto 9
        this.xpoints[9] = x - nuevoAncho;
        this.ypoints[9] = y - 3*nuevoAlto;
        
        //Punto 10
        this.xpoints[10] = x - nuevoAncho;
        this.ypoints[10] = y - nuevoAlto;
        
        //Punto 11
        this.xpoints[11] = x - 3*nuevoAncho;
        this.ypoints[11] = y - nuevoAlto;
        
    }

}
