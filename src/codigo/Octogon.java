/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author joseignacionavassanz
 */
public class Octogon extends Polygon {
    
    public Color miColor = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    public int x = 0;
    public int y = 0;

    //Constructor de la clase
    public Octogon (int _x, int _y, int _width, int _height, Color _miColor, boolean _relleno) {
                
        /*
        Definimos los puntos X e Y
        Mediante this podemos apuntar 
        a las variables de instancia
        y decirles el nuevo valor que
        van a tomar.
         */
        this.x = _x;
        this.y = _y;
        
        //Puntos del Octógono
        
        int ancho = _width;
        int alto = _height;
        
        /*
        Puntos del Octógono
                _________
               /3       4\
              /           \
             /             \
            |2             5|
            |               |
            |1             6|
             \             /
              \           /
               \0_______7/
        
        */
        
        
        //Punto 0
        addPoint(_x - (ancho/4), _y - (ancho/2));
        //Punto 1
        addPoint(_x - (ancho/2), _y - (ancho/4));
        //Punto 2
        addPoint(_x - (ancho/2), _y + (ancho/4));
        //Punto 3
        addPoint(_x - (ancho/4), _y + (ancho/2));
        //Punto 4
        addPoint(_x + ancho/4, _y + ancho/2);
        //Punto 5
        addPoint(_x + ancho/2, _y + ancho/4);
        //Punto 6
        addPoint(_x + ancho/2, _y - ancho/4);
        //Punto 7
        addPoint(_x + ancho/4, _y - ancho/2);
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;
    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }
    
    public void redimensiona (int nuevoAncho, int nuevoAlto) {
        
        this.xpoints[0] = x - nuevoAncho/4;
        this.ypoints[0] = y - nuevoAlto/2;
        
        this.xpoints[1] = x - nuevoAncho/2;
        this.ypoints[1] = y - nuevoAlto/4;
        
        this.xpoints[2] = x - nuevoAncho/2;
        this.ypoints[2] = y + nuevoAlto/4;
        
        this.xpoints[3] = x - nuevoAncho/4;
        this.ypoints[3] = y + nuevoAlto/2;
        
        this.xpoints[4] = x + nuevoAncho/4;
        this.ypoints[4] = y + nuevoAlto/2;
        
        this.xpoints[5] = x + nuevoAncho/2;
        this.ypoints[5] = y + nuevoAlto/4;
        
        this.xpoints[6] = x + nuevoAncho/2;
        this.ypoints[6] = y - nuevoAlto/4;
        
        this.xpoints[7] = x + nuevoAncho/4;
        this.ypoints[7] = y - nuevoAlto/2;
       
    }
}