/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author xp
 */
public class Estrella extends Polygon{
    
    //Variable de color publica, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color color = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    public int x = 0;
    public int y = 0;
    
    public int width = 0;
    
    public Estrella (int _x, int _y, int _width,  Color _color, boolean _relleno){
        //Numero de puntos de la figura
        
        this.x=_x;
        this.y=_y;
        
        this.width=_width/12;
        
        // Codigo Pentagono
       /*
        for(int i=0; i<6; i++){
        addPoint((int) (_x + _width * Math.cos(i * 2 * Math.PI /5)),
        (int) (_y + _width * Math.sin(i*2 * Math.PI / 5 )));
        }
        */
        
       //Estrella Rellena parcialmente
       addPoint((int) (_x + _width * Math.cos(0 * 2 * Math.PI /5)),
                (int) (_y + _width * Math.sin(0*2 * Math.PI / 5 )));
       addPoint((int) (_x + _width * Math.cos(2 * 2 * Math.PI /5)),
                (int) (_y + _width * Math.sin(2*2 * Math.PI / 5 )));
       addPoint((int) (_x + _width * Math.cos(4 * 2 * Math.PI /5)),
                (int) (_y + _width * Math.sin(4*2 * Math.PI / 5 )));
       addPoint((int) (_x + _width * Math.cos(1 * 2 * Math.PI /5)),
                (int) (_y + _width * Math.sin(1*2 * Math.PI / 5 )));
       addPoint((int) (_x + _width * Math.cos(3 * 2 * Math.PI /5)),
                (int) (_y + _width * Math.sin(3*2 * Math.PI / 5 )));
        
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.color = _color;
        this.relleno = _relleno;
        
    }
    
    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
    */
    public void pintaYColorea(Graphics2D g2){
            g2.setColor(this.color);
            if (this.relleno) {
                g2.fill(this);
            } else {
                g2.draw(this);
            }
    }
        
    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
    */
    public void redimensiona (int nuevoAncho, int nuevoAlto){
        
       this.xpoints[0] = (int) (x + nuevoAncho * Math.cos(0 * 2 * Math.PI /5));
       this.ypoints[0] = (int) (y + nuevoAlto * Math.sin(0*2 * Math.PI / 5 ));
       
       this.xpoints[1] = (int) (x + nuevoAncho * Math.cos(2 * 2 * Math.PI /5));
       this.ypoints[1] = (int) (y + nuevoAlto * Math.sin(2*2 * Math.PI / 5 ));
       
       this.xpoints[2] = (int) (x + nuevoAncho * Math.cos(4 * 2 * Math.PI /5));
       this.ypoints[2] = (int) (y + nuevoAlto * Math.sin(4*2 * Math.PI / 5 ));
       
       this.xpoints[3] = (int) (x + nuevoAncho * Math.cos(1 * 2 * Math.PI /5));
       this.ypoints[3] = (int) (y + nuevoAlto * Math.sin(1*2 * Math.PI / 5 ));
       
       this.xpoints[4] = (int) (x + nuevoAncho * Math.cos(3 * 2 * Math.PI /5));
       this.ypoints[4] = (int) (y + nuevoAlto * Math.sin(3*2 * Math.PI / 5 ));
       
    }
    
}
