/*
 * Mi cuarta clase JAVA

 La clase Rombo permite almacenar un rombo
 además de su posición en la pantalla. También
 permite saber si está coloreado o no y en
 que color está coloreado en caso de estarlo
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author Jose Ignacio Navas Sanz
 */
public class Rombo extends Polygon {

    public Color miColor = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    int x = 0;
    int y = 0;

    //Constructor de la clase
    public Rombo(int _x, int _y, int _width, int _height, Color _miColor, boolean _relleno) {

        npoints = 4;

        this.x = _x;
        this.y = _y;

        this.xpoints[0] = _x;
        this.ypoints[0] = _y - (_height);

        this.xpoints[1] = _x + (_width);
        this.ypoints[1] = _y;

        this.xpoints[2] = _x;
        this.ypoints[2] = _y + (_height);

        this.xpoints[3] = _x - (_width);
        this.ypoints[3] = _y;
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;

    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAlto) {

        this.xpoints[0] = x;
        this.ypoints[0] = y - (nuevoAlto);

        this.xpoints[1] = x + (nuevoAlto / 2);
        this.ypoints[1] = y;

        this.xpoints[2] = x;
        this.ypoints[2] = y + (nuevoAlto);

        this.xpoints[3] = x - (nuevoAlto / 2);
        this.ypoints[3] = y;

    }

}
