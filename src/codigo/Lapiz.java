/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author joseignacionavassanz
 */
public class Lapiz extends Polygon{
    
    //Variable de color publica, para llamarla en VentanaDibujo
    //Inicializada a null, le daremos el color en VentanaDibujo
    public Color miColor = null;
    public boolean relleno = false;
    
    int x = 0;
    int y = 0;
    
    //Constructor de la clase
    public Lapiz (int _x, int _y, Color _miColor, boolean _relleno) {
       
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;

    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

}