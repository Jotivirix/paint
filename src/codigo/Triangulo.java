/*
 * Mi segunda clase JAVA

 La clase Triángulo permite almacenar un triángulo
 además de su psoción en la pantalla. También
 permite saber si está coloreado o no y en que 
 color está coloreado en caso de estarlo
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author Jose Ignacio Navas Sanz
 */
public class Triangulo extends Polygon {

    public Color miColor = null;
    public boolean relleno = false;
    
    //Variables para poder hacer la redimensión
    public int x = 0;
    public int y = 0;

    //Constructor de la clase.
    public Triangulo(int _x, int _y, int _width, Color _miColor, boolean _relleno) {

        /* 
        Al ser un triángulo y quererlo hacer equilátero
        sabemos que la base es la altura por 2 por lo tanto
        lo especificamos con la variable ancho de tipo entero
         */
        int ancho = _width * 2;

        /*
        Definimos los puntos X e Y
        Mediante this podemos apuntar 
        a las variables de instancia
        y decirles el nuevo valor que
        van a tomar.
         */
        this.x = _x;
        this.y = _y;

        //Definimos el número de puntos del Triángulo
        this.npoints = 3;

        /*
        Definimos Todos Los Puntos del Triángulo
        
               Punto0
                   / \
                  /   \
           Punto2/_____\Punto1
        
         */
        this.xpoints[0] = _x;
        this.ypoints[0] = _y - (ancho / 4);

        this.xpoints[1] = _x + (ancho / 3);
        this.ypoints[1] = _y + (ancho / 3);

        this.xpoints[2] = _x - (ancho / 3);
        this.ypoints[2] = _y + (ancho / 3);
        
        /*
        Establecemos el nuevo valor del 
        color y del relleno que tendrá
        */
        this.miColor = _miColor;
        this.relleno = _relleno;
    }

    //Método pintaYColorea
    /*
    Mediante este método elegimos
    el color del relleno que tendrá
    la figura dibujada.
     */
    public void pintaYColorea(Graphics2D g2) {
        //Determino el color del círculo
        g2.setColor(this.miColor);
        //Miro si está o no relleno y en función de eso lo relleno o no
        if (this.relleno) {
            g2.fill(this);
        } else {
            g2.draw(this);
        }
    }

    //Método redimensiona
    /*
    Mediante este método especificaremos en la 
    clase principal VentanaDibujo como se deben
    redimensionar las figuras en función de la
    distancia arrastrada con el ratón sin soltar el click
     */
    public void redimensiona(int nuevoAncho, int nuevoAlto) {

        this.xpoints[0] = x;
        this.ypoints[0] = y - (nuevoAlto / 4);

        this.xpoints[1] = x + (nuevoAncho / 3);
        this.ypoints[1] = y + (nuevoAlto / 3);

        this.xpoints[2] = x - (nuevoAncho / 3);
        this.ypoints[2] = y + (nuevoAlto / 3);

    }

}
