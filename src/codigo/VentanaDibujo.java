/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

//import com.sun.javafx.css.CalculatedValue;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.KeyEvent;
//import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
//import java.nio.Buffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * Autor: Jose Ignacio Navas Sanz
 *
 * CURSO: 1º DAM
 *
 * VentanaDibujo.java ejecuta un jFrame mediante el cual haremos una recreación
 * del programa Paint
 *
 * En nuestro caso, hemos añadido una serie de figuras, y línea y lápiz para
 * trazo libre También hemos añadido botones de deshacer, de borrar todo y un
 * borrador para borrar parcialmente figuras.
 *
 * RAMA MASTER
 * Posible para entregar
 *
 */
public class VentanaDibujo extends javax.swing.JFrame {

    //private static int DIMENSION_ARRAY = 25;
    //Imagen en la que pintaremos los circulos
    BufferedImage buffer = null;

    //Ahora en esta versión utilizaré un ArrayList
    ArrayList listaFormas = new ArrayList();

    //BufferedImage[] listaDeshacer = new BufferedImage[DIMENSION_ARRAY];
    //Número indicador de la forma. Por defecto 0, los círculos
    int forma = 0;

    //Variable para almacenar la posición en la que se dibuja la forma
    int posX = 0;
    int posY = 0;

    //Declaramos el color por defecto
    Color colorDefecto = Color.BLACK;

    //Booleano para saber si está relleno
    boolean relleno = false;

    /**
     * Creates new form VenatanaDibujo
     */
    public VentanaDibujo() {
        initComponents();
        centrar(this);
        //Creamos el buffer del tamaño del jPanel1
        buffer = (BufferedImage) jPanel1.createImage(jPanel1.getWidth(), jPanel1.getHeight());
        //Habilitamos el buffer para poder dibujar en él
        buffer.createGraphics();
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, jPanel1.getWidth(), jPanel1.getHeight());
        valorSlider.setText(String.valueOf(jSlider1.getValue()));
    }

    //Método chequeaPunto
    /*
    Comprueba si al hacer click en algún punto de la pantalla
    ya hay una figura y de ser así luego implementaremos una
    función que nos impide pintar encima.
    A fecha 3 de Marzo, no usamos este método al estar implementado
    el borrador y querer pintar este dentro de las figuras 
     */
    private boolean chequeaPunto(int x, int y) {
        boolean contiene = false;
        for (int i = 0; i < listaFormas.size(); i++) {
            if (((Shape) listaFormas.get(i)).contains(x, y)) {
                /*
                Si en algún momento el contains devuelve true
                es por que el punto elegido está en alguna
                forma de las previamente definidas en el paint
                 */
                contiene = true;
            }
        }
        return contiene;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        //Establezco el color del buffer
        g2.setColor(Color.white);
        //Relleno el buffer del color elegido
        g2.fillRect(0, 0, jPanel1.getWidth(), jPanel1.getHeight());

        //Leemos las figuras
        //Recorreremos el tipo con un bucle que recorra el ArrayList
        for (int i = 0; i < listaFormas.size(); i++) {
            /*
            En funcion de la forma que elijamos, de las que estén
            disponibles en el programa, pintaremos una forma u otra.
            Para ello utilizamos sentencias if e if else hasta acabar
            recorriendo todos los tipos de figura disponibles.
             */
            if (listaFormas.get(i) instanceof Circulo) {
                ((Circulo) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Triangulo) {
                ((Triangulo) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Cuadrado) {
                ((Cuadrado) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Rombo) {
                ((Rombo) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Cruz) {
                ((Cruz) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Estrella) {
                ((Estrella) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Pentagono) {
                ((Pentagono) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Linea) {
                ((Linea) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Octogon) {
                ((Octogon) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Hexagon) {
                ((Hexagon) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Lapiz) {
                ((Lapiz) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Goma) {
                ((Goma) listaFormas.get(i)).pintaYColorea(g2);
            } else if (listaFormas.get(i) instanceof Spray) {
                ((Spray) listaFormas.get(i)).pintaYColorea(g2);
            }
        }
        //Apunto al jPanel1
        g2 = (Graphics2D) jPanel1.getGraphics();
        g2.drawImage(buffer, 0, 0, jPanel1.getWidth(), jPanel1.getHeight(), null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DialogoSelectorColores = new javax.swing.JDialog();
        jColorChooser1 = new javax.swing.JColorChooser();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        DialogoSelectorArchivos = new javax.swing.JDialog();
        jFileChooser1 = new javax.swing.JFileChooser();
        DialogoLapizLento = new javax.swing.JDialog();
        AceptarRiesgo = new javax.swing.JButton();
        CancelarRiesgo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        DialogoGuardarPulsadoNuevo = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        GuardarNuevo = new javax.swing.JButton();
        AceptarNuevoBorrar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jSlider1 = new javax.swing.JSlider();
        valorSlider = new javax.swing.JTextField();
        BotonCirculo = new javax.swing.JButton();
        BotonTriangulo = new javax.swing.JButton();
        BotonRombo = new javax.swing.JButton();
        BotonCuadrado = new javax.swing.JButton();
        BotonCruz = new javax.swing.JButton();
        BotonEstrella = new javax.swing.JButton();
        BotonLinea = new javax.swing.JButton();
        BotonPentagono = new javax.swing.JButton();
        BotonOctogono = new javax.swing.JButton();
        BotonHexagono = new javax.swing.JButton();
        Colores = new javax.swing.JButton();
        FilledON = new javax.swing.JRadioButton();
        BorradoBoton = new javax.swing.JButton();
        BotonLapiz = new javax.swing.JButton();
        DeshacerBoton = new javax.swing.JButton();
        BotonGoma = new javax.swing.JButton();
        SprayBoton = new javax.swing.JButton();
        Nuevo = new javax.swing.JButton();
        MenuPaint = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AbrirMac = new javax.swing.JMenuItem();
        AbrirWindows = new javax.swing.JMenuItem();
        GuardarMac = new javax.swing.JMenuItem();
        GuardarWin = new javax.swing.JMenuItem();
        DeshacerMac = new javax.swing.JMenuItem();
        DeshacerWindows = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        DialogoSelectorColores.setMinimumSize(new java.awt.Dimension(1130, 650));
        DialogoSelectorColores.setSize(new java.awt.Dimension(1130, 650));

        jButton4.setText("ACEPTAR");
        jButton4.setMaximumSize(new java.awt.Dimension(100, 30));
        jButton4.setMinimumSize(new java.awt.Dimension(100, 30));
        jButton4.setPreferredSize(new java.awt.Dimension(100, 30));
        jButton4.setSize(new java.awt.Dimension(100, 30));
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton4MousePressed(evt);
            }
        });

        jButton6.setText("CANCELAR");
        jButton6.setMaximumSize(new java.awt.Dimension(100, 30));
        jButton6.setMinimumSize(new java.awt.Dimension(100, 30));
        jButton6.setPreferredSize(new java.awt.Dimension(100, 30));
        jButton6.setSize(new java.awt.Dimension(100, 30));

        javax.swing.GroupLayout DialogoSelectorColoresLayout = new javax.swing.GroupLayout(DialogoSelectorColores.getContentPane());
        DialogoSelectorColores.getContentPane().setLayout(DialogoSelectorColoresLayout);
        DialogoSelectorColoresLayout.setHorizontalGroup(
            DialogoSelectorColoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoSelectorColoresLayout.createSequentialGroup()
                .addGap(353, 353, 353)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(202, 202, 202)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(374, Short.MAX_VALUE))
            .addGroup(DialogoSelectorColoresLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jColorChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        DialogoSelectorColoresLayout.setVerticalGroup(
            DialogoSelectorColoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoSelectorColoresLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jColorChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(DialogoSelectorColoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(208, Short.MAX_VALUE))
        );

        DialogoSelectorArchivos.setSize(new java.awt.Dimension(1120, 870));

        javax.swing.GroupLayout DialogoSelectorArchivosLayout = new javax.swing.GroupLayout(DialogoSelectorArchivos.getContentPane());
        DialogoSelectorArchivos.getContentPane().setLayout(DialogoSelectorArchivosLayout);
        DialogoSelectorArchivosLayout.setHorizontalGroup(
            DialogoSelectorArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jFileChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 866, Short.MAX_VALUE)
        );
        DialogoSelectorArchivosLayout.setVerticalGroup(
            DialogoSelectorArchivosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoSelectorArchivosLayout.createSequentialGroup()
                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 266, Short.MAX_VALUE))
        );

        AceptarRiesgo.setFont(new java.awt.Font("Lucida Grande", 3, 48)); // NOI18N
        AceptarRiesgo.setText("ACEPTAR");
        AceptarRiesgo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                AceptarRiesgoMousePressed(evt);
            }
        });

        CancelarRiesgo.setFont(new java.awt.Font("Lucida Grande", 3, 48)); // NOI18N
        CancelarRiesgo.setText("CANCELAR");
        CancelarRiesgo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CancelarRiesgoMousePressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Tenga precaución al utilizar este modo de lápiz");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Utilice esta herramienta bajo su propia responsabilidad");

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Debe utilizar este lápiz sabiendo que,");

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("si se desplaza muy rápido, se añadirán");

        jLabel7.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("los puntos muy separados en el jPanel");

        jLabel8.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Si pulsa ACEPTAR, podrá utilizar este tipo de lápiz");

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Por el contrario, si pulsa CANCELAR volverá a dibujar círculos");

        jLabel10.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("No nos hacemos responsables de las quejas. Está advertido");

        jLabel11.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("pero podrá seguir cambiando la figura como venía haciendo");

        jLabel12.setFont(new java.awt.Font("Lucida Grande", 0, 36)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Si pulsa CANCELAR, se dibujarán círculos por defecto");

        javax.swing.GroupLayout DialogoLapizLentoLayout = new javax.swing.GroupLayout(DialogoLapizLento.getContentPane());
        DialogoLapizLento.getContentPane().setLayout(DialogoLapizLentoLayout);
        DialogoLapizLentoLayout.setHorizontalGroup(
            DialogoLapizLentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoLapizLentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DialogoLapizLentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 1133, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DialogoLapizLentoLayout.createSequentialGroup()
                        .addComponent(AceptarRiesgo, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CancelarRiesgo, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        DialogoLapizLentoLayout.setVerticalGroup(
            DialogoLapizLentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DialogoLapizLentoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(DialogoLapizLentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CancelarRiesgo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AceptarRiesgo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(149, 149, 149))
        );

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 2, 30)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("¿ESTÁS SEGURO QUE QUIERES HACER ESO?");

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 2, 30)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("SI QUIERES GUARDAR EL DIBUJO, PULSA GUARDAR");

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 2, 30)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("SI TE DA IGUAL, PULSA ACEPTAR");

        GuardarNuevo.setText("GUARDAR");
        GuardarNuevo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                GuardarNuevoMousePressed(evt);
            }
        });

        AceptarNuevoBorrar.setText("ACEPTAR");
        AceptarNuevoBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                AceptarNuevoBorrarMousePressed(evt);
            }
        });

        javax.swing.GroupLayout DialogoGuardarPulsadoNuevoLayout = new javax.swing.GroupLayout(DialogoGuardarPulsadoNuevo.getContentPane());
        DialogoGuardarPulsadoNuevo.getContentPane().setLayout(DialogoGuardarPulsadoNuevoLayout);
        DialogoGuardarPulsadoNuevoLayout.setHorizontalGroup(
            DialogoGuardarPulsadoNuevoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoGuardarPulsadoNuevoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DialogoGuardarPulsadoNuevoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(DialogoGuardarPulsadoNuevoLayout.createSequentialGroup()
                        .addComponent(GuardarNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(AceptarNuevoBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogoGuardarPulsadoNuevoLayout.createSequentialGroup()
                        .addGroup(DialogoGuardarPulsadoNuevoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 1172, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1172, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        DialogoGuardarPulsadoNuevoLayout.setVerticalGroup(
            DialogoGuardarPulsadoNuevoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoGuardarPulsadoNuevoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(DialogoGuardarPulsadoNuevoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AceptarNuevoBorrar, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
                    .addComponent(GuardarNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(0, 0));
        setMaximumSize(new java.awt.Dimension(1600, 1000));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        setResizable(false);
        setSize(new java.awt.Dimension(1280, 720));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(0, 720));
        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 724, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(51, 153, 255));

        jSlider1.setMaximum(400);
        jSlider1.setValue(30);
        jSlider1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jSlider1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jSlider1MouseDragged(evt);
            }
        });
        jSlider1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jSlider1MousePressed(evt);
            }
        });

        valorSlider.setText("30");
        valorSlider.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                valorSliderKeyPressed(evt);
            }
        });

        BotonCirculo.setBackground(new java.awt.Color(255, 255, 255));
        BotonCirculo.setFont(new java.awt.Font("Lucida Grande", 0, 32)); // NOI18N
        BotonCirculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulopaint.png"))); // NOI18N
        BotonCirculo.setText("●");
        BotonCirculo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonCirculo.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonCirculo.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonCirculo.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonCirculo.setSize(new java.awt.Dimension(45, 47));
        BotonCirculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonCirculoMousePressed(evt);
            }
        });

        BotonTriangulo.setBackground(new java.awt.Color(255, 255, 255));
        BotonTriangulo.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonTriangulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/triangulo.png"))); // NOI18N
        BotonTriangulo.setText("▲");
        BotonTriangulo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonTriangulo.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonTriangulo.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonTriangulo.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonTriangulo.setSize(new java.awt.Dimension(45, 47));
        BotonTriangulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonTrianguloMousePressed(evt);
            }
        });

        BotonRombo.setBackground(new java.awt.Color(255, 255, 255));
        BotonRombo.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonRombo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/rombo.jpeg"))); // NOI18N
        BotonRombo.setText("♦");
        BotonRombo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonRombo.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonRombo.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonRombo.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonRombo.setSize(new java.awt.Dimension(45, 47));
        BotonRombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonRomboMousePressed(evt);
            }
        });

        BotonCuadrado.setBackground(new java.awt.Color(255, 255, 255));
        BotonCuadrado.setFont(new java.awt.Font("Lucida Grande", 0, 34)); // NOI18N
        BotonCuadrado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cuadrado.png"))); // NOI18N
        BotonCuadrado.setText("■");
        BotonCuadrado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonCuadrado.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonCuadrado.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonCuadrado.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonCuadrado.setSize(new java.awt.Dimension(45, 47));
        BotonCuadrado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonCuadradoMousePressed(evt);
            }
        });

        BotonCruz.setBackground(new java.awt.Color(255, 255, 255));
        BotonCruz.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonCruz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cruz.png"))); // NOI18N
        BotonCruz.setText("✙");
        BotonCruz.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonCruz.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonCruz.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonCruz.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonCruz.setSize(new java.awt.Dimension(45, 47));
        BotonCruz.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonCruzMousePressed(evt);
            }
        });

        BotonEstrella.setBackground(new java.awt.Color(255, 255, 255));
        BotonEstrella.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonEstrella.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/estrella.png"))); // NOI18N
        BotonEstrella.setText("★");
        BotonEstrella.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonEstrella.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonEstrella.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonEstrella.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonEstrella.setSize(new java.awt.Dimension(45, 47));
        BotonEstrella.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonEstrellaMousePressed(evt);
            }
        });

        BotonLinea.setBackground(new java.awt.Color(255, 255, 255));
        BotonLinea.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonLinea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/linea.png"))); // NOI18N
        BotonLinea.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonLinea.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonLinea.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonLinea.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonLinea.setSize(new java.awt.Dimension(45, 47));
        BotonLinea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonLineaMousePressed(evt);
            }
        });

        BotonPentagono.setBackground(new java.awt.Color(255, 255, 255));
        BotonPentagono.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonPentagono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pentagono.png"))); // NOI18N
        BotonPentagono.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonPentagono.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonPentagono.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonPentagono.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonPentagono.setSize(new java.awt.Dimension(45, 47));
        BotonPentagono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonPentagonoMousePressed(evt);
            }
        });

        BotonOctogono.setBackground(new java.awt.Color(255, 255, 255));
        BotonOctogono.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        BotonOctogono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/octogono.jpg"))); // NOI18N
        BotonOctogono.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonOctogono.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonOctogono.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonOctogono.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonOctogono.setSize(new java.awt.Dimension(45, 47));
        BotonOctogono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonOctogonoMousePressed(evt);
            }
        });

        BotonHexagono.setBackground(new java.awt.Color(255, 255, 255));
        BotonHexagono.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonHexagono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hexagono.jpg"))); // NOI18N
        BotonHexagono.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonHexagono.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonHexagono.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonHexagono.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonHexagono.setSize(new java.awt.Dimension(45, 47));
        BotonHexagono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonHexagonoMousePressed(evt);
            }
        });

        Colores.setBackground(new java.awt.Color(255, 255, 255));
        Colores.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        Colores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/colores.png"))); // NOI18N
        Colores.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Colores.setPreferredSize(new java.awt.Dimension(95, 47));
        Colores.setSize(new java.awt.Dimension(95, 47));
        Colores.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Colores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                ColoresMousePressed(evt);
            }
        });

        FilledON.setBackground(new java.awt.Color(255, 255, 255));
        FilledON.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        FilledON.setForeground(new java.awt.Color(255, 255, 255));
        FilledON.setText("Relleno");
        FilledON.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        FilledON.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                FilledONMousePressed(evt);
            }
        });

        BorradoBoton.setBackground(new java.awt.Color(255, 255, 255));
        BorradoBoton.setFont(new java.awt.Font("Lucida Grande", 0, 5)); // NOI18N
        BorradoBoton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/borrartodo.png"))); // NOI18N
        BorradoBoton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BorradoBoton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        BorradoBoton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BorradoBotonMousePressed(evt);
            }
        });

        BotonLapiz.setBackground(new java.awt.Color(255, 255, 255));
        BotonLapiz.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonLapiz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lapiz.png"))); // NOI18N
        BotonLapiz.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonLapiz.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonLapiz.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonLapiz.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonLapiz.setSize(new java.awt.Dimension(45, 47));
        BotonLapiz.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonLapizMousePressed(evt);
            }
        });

        DeshacerBoton.setBackground(new java.awt.Color(255, 255, 255));
        DeshacerBoton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/deshacer.png"))); // NOI18N
        DeshacerBoton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        DeshacerBoton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        DeshacerBoton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DeshacerBotonMousePressed(evt);
            }
        });

        BotonGoma.setBackground(new java.awt.Color(255, 255, 255));
        BotonGoma.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        BotonGoma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/goma.jpg"))); // NOI18N
        BotonGoma.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BotonGoma.setMaximumSize(new java.awt.Dimension(45, 47));
        BotonGoma.setMinimumSize(new java.awt.Dimension(45, 47));
        BotonGoma.setPreferredSize(new java.awt.Dimension(45, 47));
        BotonGoma.setSize(new java.awt.Dimension(45, 47));
        BotonGoma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                BotonGomaMousePressed(evt);
            }
        });

        SprayBoton.setBackground(new java.awt.Color(255, 255, 255));
        SprayBoton.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        SprayBoton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/spray.jpg"))); // NOI18N
        SprayBoton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        SprayBoton.setMaximumSize(new java.awt.Dimension(45, 47));
        SprayBoton.setMinimumSize(new java.awt.Dimension(45, 47));
        SprayBoton.setPreferredSize(new java.awt.Dimension(45, 47));
        SprayBoton.setSize(new java.awt.Dimension(45, 47));
        SprayBoton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SprayBotonMousePressed(evt);
            }
        });

        Nuevo.setBackground(new java.awt.Color(255, 255, 255));
        Nuevo.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        Nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo.png"))); // NOI18N
        Nuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Nuevo.setPreferredSize(new java.awt.Dimension(95, 47));
        Nuevo.setSize(new java.awt.Dimension(95, 47));
        Nuevo.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        Nuevo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Nuevo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                NuevoMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(valorSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BotonCirculo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotonTriangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(BotonCuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonCruz, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonPentagono, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonOctogono, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(BotonRombo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonEstrella, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonHexagono, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BotonLapiz, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(BotonGoma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SprayBoton, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 136, Short.MAX_VALUE)
                .addComponent(FilledON)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Nuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Colores, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(DeshacerBoton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BorradoBoton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(DeshacerBoton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(FilledON, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(BotonHexagono, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotonLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotonEstrella, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(BotonCirculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BotonTriangulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(valorSlider)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(BotonCuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BotonRombo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(BotonPentagono, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BotonCruz, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(1, 1, 1)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(BotonOctogono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(BotonGoma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGap(5, 5, 5)
                            .addComponent(BotonLapiz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(SprayBoton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(Nuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Colores, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addComponent(BorradoBoton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jMenu1.setText("Archivo");

        AbrirMac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.META_MASK));
        AbrirMac.setText("Abrir MAC");
        AbrirMac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirMacActionPerformed(evt);
            }
        });
        jMenu1.add(AbrirMac);

        AbrirWindows.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        AbrirWindows.setText("Abrir WIN");
        AbrirWindows.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirWindowsActionPerformed(evt);
            }
        });
        jMenu1.add(AbrirWindows);

        GuardarMac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.META_MASK));
        GuardarMac.setText("Guardar MAC");
        GuardarMac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarMacActionPerformed(evt);
            }
        });
        jMenu1.add(GuardarMac);

        GuardarWin.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        GuardarWin.setText("Guardar WIN");
        GuardarWin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarWinActionPerformed(evt);
            }
        });
        jMenu1.add(GuardarWin);

        DeshacerMac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.META_MASK));
        DeshacerMac.setText("Deshacer MAC");
        DeshacerMac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeshacerMacActionPerformed(evt);
            }
        });
        jMenu1.add(DeshacerMac);

        DeshacerWindows.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        DeshacerWindows.setText("Deshacer WIN");
        DeshacerWindows.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeshacerWindowsActionPerformed(evt);
            }
        });
        jMenu1.add(DeshacerWindows);

        MenuPaint.add(jMenu1);

        jMenu2.setText("Editar");
        MenuPaint.add(jMenu2);

        setJMenuBar(MenuPaint);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1180, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
        /*
        Esto hacía que no pudieses pintar figuras dentro de otras
        if (chequeaPunto(evt.getX(), evt.getY()) && forma!=11) {
            System.out.println("No puedes pintar encima");
        }
         */
        //Declaro el ancho del Rombo para ahorrar longitud de código más abajo
        int anchoRombo = jSlider1.getValue() / 2;
        /*
        En función de la forma elegida, cada botón tiene la suya
        haremos una u otra figura. Para ir cambiando de figura,
        vamos avanzando con un switch que vaya cambiando la
        figura que se deberá pintar en el jPanel.
        Todas las figuras se pintan a partir del lugar donde
        hemos hecho click, y tienen definido un color de borde,
        por defecto negro, y relleno, por defecto establecido
        a falso. El ancho y el alto de las figuras, lo determina
        el valor del jSlider en el momento que las pintamos.
         */
        switch (forma) {
            //Si la forma es 0, pinto círculos
            case 0: {
                listaFormas.add(new Circulo(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 1, pintamos triángulos
            case 1: {
                listaFormas.add(new Triangulo(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 2, pintamos cuadrados
            case 2: {
                listaFormas.add(new Cuadrado(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 3 pintamos rombos
            case 3: {
                listaFormas.add(new Rombo(evt.getX(), evt.getY(), anchoRombo, jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 4 pintamos cruces
            case 4: {
                listaFormas.add(new Cruz(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 5 pintamos Estrellas
            case 5: {
                listaFormas.add(new Estrella(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 6 pintamos Pentágonos
            case 6: {
                listaFormas.add(new Pentagono(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 7 pintaremos Líneas
            case 7: {
                listaFormas.add(new Linea(evt.getX(), evt.getY(), jSlider1.getValue(), colorDefecto));
            }
            break;
            //Si la forma vale 8 pintaremos Octógonos
            case 8: {
                listaFormas.add(new Octogon(evt.getX(), evt.getY(), jSlider1.getValue(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si la forma vale 9 pintaremos Hexágonos
            case 9: {
                listaFormas.add(new Hexagon(evt.getX(), evt.getY(), jSlider1.getValue(), jSlider1.getValue(), colorDefecto, relleno));
            }
            break;
            //Si en este caso, la forma vale 10, dibujaremos con un lápiz
            //No es un lápiz perfecto al extender a Polygon por lo que nos
            //cerrará la figura desde el primer punto hasta el último con 
            //una línea.
            case 10: {
                listaFormas.add(new Lapiz(evt.getX(), evt.getY(), colorDefecto, relleno));
            }
            break;
            //Si la forma es 11, pintaremos círculos pequeños a modo de goma
            case 11: {
                listaFormas.add(new Goma(evt.getX(), evt.getY(), jSlider1.getValue(), Color.WHITE, true));
            }
            break;
            //Si la forma es 12, pintaremos con el lapiz de advertencia
            case 12: {
                listaFormas.add(new Spray(evt.getX(), evt.getY(), jSlider1.getValue() / 2, colorDefecto, true));
                repaint();
            }
            break;
        }
        //Volvemos a pintar en el jPanel
        repaint();

    }//GEN-LAST:event_jPanel1MousePressed

    private void DeshacerBotonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeshacerBotonMousePressed
        /*
        Si el índice está por encima de 0, podremos borrar, si no
        borraríamos la primera pantalla y nos saltará un mensaje de error
         */
        if (listaFormas.size() > 0) {
            listaFormas.remove(listaFormas.size() - 1);
            repaint();
        } else {
            JOptionPane.showMessageDialog(null, "No se puede deshacer aún", "NO HA DIBUJADO", 2);
        }
    }//GEN-LAST:event_DeshacerBotonMousePressed

    private void BorradoBotonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BorradoBotonMousePressed
        /*
        Añadimos un botón de borrar todo a la vez
        Y volvemos a repintar la pantalla en blanco
         */
        if (listaFormas.size() > 0) {
            listaFormas.clear();
            repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Necesitas dibujar", "Error", 0);
        }
    }//GEN-LAST:event_BorradoBotonMousePressed

    private void jSlider1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSlider1MouseDragged
        //Si arrastro el Slider, su valor irá cambiando a medida que arrastro
        valorSlider.setText(String.valueOf(jSlider1.getValue()));
    }//GEN-LAST:event_jSlider1MouseDragged

    private void jSlider1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSlider1MousePressed
        //Si hago click en cualquier sitio del Slider, cambiará su valor
        valorSlider.setText(String.valueOf(jSlider1.getValue()));
    }//GEN-LAST:event_jSlider1MousePressed

    private void BotonCirculoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonCirculoMousePressed
        //Si aprieto el Circulo, la forma será el 0
        forma = 0;
    }//GEN-LAST:event_BotonCirculoMousePressed

    private void BotonTrianguloMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonTrianguloMousePressed
        //Si aprieto el triángulo, la forma será el 1
        forma = 1;
    }//GEN-LAST:event_BotonTrianguloMousePressed

    private void ColoresMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ColoresMousePressed
        /*
        Al presionar el botoón selector de colores se abrirá una
        nueva ventana dándonos a elegir el color que queremos usar
         */
        DialogoSelectorColores.setSize(jPanel1.getWidth(), jPanel1.getHeight());
        DialogoSelectorColores.setVisible(true);
    }//GEN-LAST:event_ColoresMousePressed

    private void jButton4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MousePressed
        //Al presionar el boton aceptar de los colores se cerrará
        colorDefecto = jColorChooser1.getColor();
        DialogoSelectorColores.setVisible(false);
    }//GEN-LAST:event_jButton4MousePressed

    private void BotonCuadradoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonCuadradoMousePressed
        //Si aprieto el Cuadrado, la forma será el 2
        forma = 2;
    }//GEN-LAST:event_BotonCuadradoMousePressed

    private void BotonRomboMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonRomboMousePressed
        //Si aprieto el Rombo, la forma será la 3
        forma = 3;
    }//GEN-LAST:event_BotonRomboMousePressed

    private void BotonCruzMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonCruzMousePressed
        //Si aprieto el botón de la Cruz, la forma será la 4
        forma = 4;
    }//GEN-LAST:event_BotonCruzMousePressed

    private void valorSliderKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valorSliderKeyPressed
        /*
        Mediante este método podemos escribir el valor que
        queramos en el jTextField que indica el valor actual
        del jSlider1 y al presionar la tecla ENTER ese valor
        insertado quedará guardado y pasará a ser el nuevo 
        valor del jTextField. Acelera el proceso de elegir
        el tamaño en este apartado
         */
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jSlider1.setValue(Integer.valueOf(valorSlider.getText()));
        }
    }//GEN-LAST:event_valorSliderKeyPressed

    private void BotonEstrellaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonEstrellaMousePressed
        //Si aprieto la estrella, la forma será 5
        forma = 5;
    }//GEN-LAST:event_BotonEstrellaMousePressed

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
        /*
        A medida que vaya arrastrando el ratón por el jPanel iré
        pintando en el una serie de figuras en función del número
        de caso en el que me sitúe. Se detallan los casos
         */
        switch (forma) {
            case 0: {
                //Si estoy en el caso 0 dibujaré círculos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo círculo
                Circulo aux = (Circulo) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Círculo conforme me muevo por el jPanel
                int radio = (int) (evt.getX() - aux.x);
                aux.width = radio;
                aux.height = radio;
            }
            break;
            case 1: {
                //Si estoy en el caso 1 dibujaré triángulos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Triángulo
                Triangulo aux = (Triangulo) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Triángulo conforme me muevo por el jPanel
                int base = (int) (evt.getX() - aux.x);
                int alto = (int) (evt.getY() - aux.y);
                aux.redimensiona(base, alto);
            }
            break;
            case 2: {
                //Si estoy en el caso 2 dibujaré cuadrados
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo cuadrado
                Cuadrado aux = (Cuadrado) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Cuadrado conforme me muevo por el jPanel
                int radio = (int) (evt.getX() - aux.x);
                aux.redimensiona(radio);
            }
            break;
            case 3: {
                //Si estoy en el caso 3 dibujaré rombos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Rombo
                Rombo aux = (Rombo) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Rombo conforme me muevo por el jPanel
                int alto = (int) (evt.getY() - aux.y);
                aux.redimensiona(alto);
            }
            break;
            case 4: {
                //Si estoy en el caso 4 dibujaré cruces
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Cruz
                Cruz aux = (Cruz) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono la Cruz conforme me muevo por el jPanel
                int ancho = (int) (evt.getX() - aux.x);
                aux.redimensiona(ancho, ancho);
            }
            break;
            case 5: {
                //Si estoy en el caso 5 dibujaré estrellas de 5 puntas
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Estrella
                Estrella aux = (Estrella) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono la Estrella conforme me muevo por el jPanel
                int radio = (evt.getX() - aux.x);
                int alto = (evt.getY() - aux.y);
                aux.redimensiona(radio, alto);
            }
            break;
            case 6: {
                //Si estoy en el caso 6 dibujaré pentágonos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Pentágono
                Pentagono aux = (Pentagono) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Pentágono conforme me muevo por el jPanel
                int radio = (evt.getX() - aux.x);
                aux.redimensiona(radio);
            }
            break;
            case 7: {
                //Si estoy en el caso 7 dibujaré líneas
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Línea
                Linea aux = (Linea) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono la Línea conforme me muevo por el jPanel
                int ancho = (evt.getX() - aux.x);
                int alto = (evt.getY() - aux.y);
                aux.redimensiona(ancho, alto);
            }
            break;
            case 8: {
                //Si estoy en el caso 8 dibujaré octógonos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro la figura de tipo Octógono
                Octogon aux = (Octogon) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Octógono conforme me muevo por el jPanel
                int ancho = (evt.getX() - aux.x);
                int alto = (evt.getY() - aux.y);
                aux.redimensiona(ancho, alto);
            }
            break;
            case 9: {
                //Si estoy en el caso 9 dibujaré hexágonos
                //Siempre tendré en cuenta la última figura añadida

                //Declaro una forma de tipo Hexágono
                Hexagon aux = (Hexagon) listaFormas.get(listaFormas.size() - 1);

                //Redimensiono el Hexágono conforme me muevo por el jPanel
                int ancho = (evt.getX() - aux.x);
                int alto = (evt.getY() - aux.y);
                aux.redimensiona(ancho, alto);
            }
            break;
            case 10: {
                //Si estoy en el caso 10 dibujaré con el Lápiz
                //Siempre tendré en cuenta la última figura añadida
                //Esta lápiz es raro, puesto que deriva de Polygon pero mola
                //Te cierra el trazo, si supiese arreglarlo sería perfecto

                //Declaro una forma de tipo Lápiz
                Lapiz aux = (Lapiz) listaFormas.get(listaFormas.size() - 1);
                //Voy añadiendo puntos conforme me muevo por el jPanel
                aux.addPoint(evt.getX(), evt.getY());
            }
            break;
            //Si estamos en el caso 11 dibujaremos la goma. Será blanca y está rellena
            case 11: {
                listaFormas.add(new Goma(evt.getX(), evt.getY(), jSlider1.getValue(), Color.WHITE, true));
            }
            break;
            //Si estamos en el caso 12, dibujaremos el spray
            case 12: {
                listaFormas.add(new Spray(evt.getX(), evt.getY(), jSlider1.getValue() / 2, colorDefecto, true));
                repaint();
            }
            break;
        }
        repaint();
    }//GEN-LAST:event_jPanel1MouseDragged

    //Botón de relleno
    /*
    Si pulsamos el botón las figuras se pintarán rellenas
    Si lo deseleccionamos, como está por defecto, las
    figuras se pintarán sin relleno, sólo tendrán coloreado
    el contorno.
     */
    private void FilledONMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FilledONMousePressed
        /*
        Si la forma es la 10 o la 11, es decir, lápiz o borrador,
        iremos alternando el valor del relleno, de verdadero a
        falso y de falso a verdadero. Si no, será true. Sólo
        será true/verdadero para las formas 10 y 11
         */
        if (forma != 11 || forma != 10) {
            relleno = !relleno;
        } else {
            relleno = true;
        }
    }//GEN-LAST:event_FilledONMousePressed

    private void BotonPentagonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonPentagonoMousePressed
        //Si pulsamos el boton pentágono, la forma será 6
        forma = 6;
    }//GEN-LAST:event_BotonPentagonoMousePressed

    private void BotonLineaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonLineaMousePressed
        //Si pulsamos el botón Linea, la forma será 7
        forma = 7;
    }//GEN-LAST:event_BotonLineaMousePressed

    private void BotonOctogonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonOctogonoMousePressed
        //Si pulsamos el boton octógono, la forma será 8
        forma = 8;
    }//GEN-LAST:event_BotonOctogonoMousePressed

    private void BotonHexagonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonHexagonoMousePressed
        //Si pulsamos el boton hexágono, la forma será 9
        forma = 9;
    }//GEN-LAST:event_BotonHexagonoMousePressed

    private void BotonLapizMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonLapizMousePressed
        //Si pulsamos el botón del lápiz la forma será 10
        forma = 10;
    }//GEN-LAST:event_BotonLapizMousePressed

    //Método que implementa el guardado de los dibujos CTRL+O o MENU
    private void GuardarMacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarMacActionPerformed

        //Si hemos dibujado algo podremos guardar, si no salta un mensaje de error
        if (listaFormas.size() > 0) {

            //Añadimos los filtros para el tipo de archivo
            jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .JPG", ".jpg"));
            jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .PNG", ".png"));

            //Mostramos la ventana para guardar el archivo
            int seleccion = jFileChooser1.showSaveDialog(this);

            //Mediante el switch vamos avanzando
            switch (seleccion) {
                case JFileChooser.APPROVE_OPTION: {
                    //Establecemos el fichero
                    File fichero = jFileChooser1.getSelectedFile();
                    //Le damos un nombre al fichero
                    String nombre = fichero.getName();
                    //Le añadimos la extension al fichero
                    String extension = nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length());
                    //Imprimimos por consola la extension. Solo para comprobar correcto funcionamiento
                    System.out.println(extension);

                    //Mientras la extensión sea JPG o PNG guarda el archivo
                    //Añade un mensaje que indica que el archivo se ha guardado
                    if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")) {
                        try {
                            ImageIO.write(buffer, extension, fichero);
                            JOptionPane.showMessageDialog(null, "Tu dibujo se ha guardado", "Dibujo Guardado", 1);
                        } //Si no se guarda, entramos aquí
                        catch (IOException ex) {
                            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(Level.SEVERE, null, ex);

                        }
                        //Limpiamos todo el panel
                        listaFormas.clear();
                        repaint();
                        forma = 0;
                    }
                }
                break;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Necesitas dibujar", "Error", 0);
        }
        
    }//GEN-LAST:event_GuardarMacActionPerformed

    //Método que implementa la posibilidad de abrir imágenes de nuestro ordenador
    private void AbrirMacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirMacActionPerformed

        //Elegimos las extensiones que se pueden abrir, en este caso jpg y png
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Archivos .JPG", "jpg"));
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Archivos .PNG", "png"));

        //Le decimos que nos muestre la ventana de selector de archivos
        int seleccion = jFileChooser1.showOpenDialog(this);

        switch (seleccion) {
            case JFileChooser.APPROVE_OPTION: {
                //Determinamos el archivo seleccionado
                File fichero = jFileChooser1.getSelectedFile();
                //Le asignamos un nombre
                String nombre = fichero.getName();
                //Obtenemos su extensión
                String extension = nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length());

                //Si es de extensión JPG o PNG se abrirá, si no, lanza la escepción
                if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")) {
                    try {
                        buffer = ImageIO.read(fichero);
                        repaint();
                    } catch (IOException ex) {
                        Logger.getLogger(VentanaDibujo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            break;
        }
    }//GEN-LAST:event_AbrirMacActionPerformed

    private void BotonGomaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonGomaMousePressed
        //Si pulsamos el botón de la goma la forma será la 11
        forma = 11;
    }//GEN-LAST:event_BotonGomaMousePressed

    private void SprayBotonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SprayBotonMousePressed
        /*
        Si pulsamos el botón del Spray se nos abrirá
        un cuadro de diálogo indicando que debemos de
        tomar ciertas medidas a la hora de utilizar
        el Spray
         */
        DialogoLapizLento.setSize(jPanel1.getWidth(), jPanel1.getHeight() + jPanel2.getHeight());
        DialogoLapizLento.setVisible(true);
    }//GEN-LAST:event_SprayBotonMousePressed

    private void CancelarRiesgoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CancelarRiesgoMousePressed
        //Si cancelamos las indicaciones, volvemos a pintar circulos
        //y cerramos el jDialog que indica las precacuciones a tomar
        forma = 0;
        DialogoLapizLento.setVisible(false);
    }//GEN-LAST:event_CancelarRiesgoMousePressed

    private void AceptarRiesgoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AceptarRiesgoMousePressed
        //Si por el contrario aceptamos, la forma será la 12
        //correspondiente al spray, y se cerrará la ventana
        forma = 12;
        DialogoLapizLento.setVisible(false);
    }//GEN-LAST:event_AceptarRiesgoMousePressed

    private void NuevoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_NuevoMousePressed
        //Mientras hayamos pintado algo, podremos darle a Nuevo
        //Al darle se abrirá un recuadro preguntando si queremos
        //guardar nuestro progreso, o por el contrario borrar
        if (listaFormas.size() > 0) {
            DialogoGuardarPulsadoNuevo.setSize(jPanel1.getWidth(), 300);
            DialogoGuardarPulsadoNuevo.setLocation(0, 0);
            DialogoGuardarPulsadoNuevo.setVisible(true);
        } //Si no hay nada dibujado, o hemos borrado todo, saltará un mensaje
        else {
            JOptionPane.showMessageDialog(null, "Necesitas dibujar", "Error", 0);
        }

    }//GEN-LAST:event_NuevoMousePressed

    private void AceptarNuevoBorrarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AceptarNuevoBorrarMousePressed
        //Si no guardamos el diálogo anterior, se borrará todo
        //lo que tuvíesemos dibujado y se cerrará la ventana
        listaFormas.clear();
        repaint();
        DialogoGuardarPulsadoNuevo.setVisible(false);
    }//GEN-LAST:event_AceptarNuevoBorrarMousePressed

    private void GuardarNuevoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GuardarNuevoMousePressed
        //Si por el contrario, pulsamos en guardar, se nos abrirá
        //el selector de ficheros para indicar donde queremos guardarlo

        //Añadimos los filtros para el tipo de archivo
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .JPG", ".jpg"));
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .PNG", ".png"));

        //Establecemos la ventana selectora de la ubicación final
        int seleccion = jFileChooser1.showSaveDialog(this);

        switch (seleccion) {
            case JFileChooser.APPROVE_OPTION: {
                //Declaramos el fichero a guardar
                File fichero = jFileChooser1.getSelectedFile();
                //Establecemos su nombre
                String nombre = fichero.getName();
                //Establecemos su extensión
                String extension = nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length());
                System.out.println(extension);

                //Si la extension es JPG o PNG se guardará
                if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")) {
                    try {
                        ImageIO.write(buffer, extension, fichero);
                        //Una vez guardado, nos notifica con un mensaje
                        JOptionPane.showMessageDialog(null, "Tu dibujo se ha guardado", "Dibujo Guardado", 1);
                    } //Si no tiene la extensión correcta, error
                    catch (IOException ex) {
                        java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(Level.SEVERE, null, ex);

                    }
                    //Borramos todo lo pintado hasta entonces 
                    //y volvemos a la forma por defecto, 0, circulos
                    listaFormas.clear();
                    repaint();
                    forma = 0;
                }
            }
            break;
        }
    }//GEN-LAST:event_GuardarNuevoMousePressed

    private void DeshacerMacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeshacerMacActionPerformed
        //Si hemos pintado algo y pulsamos CMD+Z
        //iremos hacia atrás. La tecla CMD solo en MAC
        if (listaFormas.size() > 0) {
            listaFormas.remove(listaFormas.size() - 1);
            repaint();
        } //SI NO HAY NADA PINTADO, LANZA UN MENSAJE
        else {
            JOptionPane.showMessageDialog(null, "No se puede deshacer aún", "NO HA DIBUJADO", 2);
        }
    }//GEN-LAST:event_DeshacerMacActionPerformed

    private void DeshacerWindowsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeshacerWindowsActionPerformed
        //Si hemos pintado algo y pulsamos CTRL+Z
        //iremos un paso hacia atrás (Pensado para WINDOWS)
        if (listaFormas.size() > 0) {
            listaFormas.remove(listaFormas.size() - 1);
            repaint();
        } //SI NO HAY NADA PINTADO, LANZA UN MENSAJE
        else {
            JOptionPane.showMessageDialog(null, "No se puede deshacer aún", "NO HA DIBUJADO", 2);
        }
    }//GEN-LAST:event_DeshacerWindowsActionPerformed

    private void AbrirWindowsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirWindowsActionPerformed
        //Elegimos las extensiones que se pueden abrir, en este caso jpg y png
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Archivos .JPG", "jpg"));
        jFileChooser1.setFileFilter(new FileNameExtensionFilter("Archivos .PNG", "png"));

        //Le decimos que nos muestre la ventana de selector de archivos
        int seleccion = jFileChooser1.showOpenDialog(this);

        switch (seleccion) {
            case JFileChooser.APPROVE_OPTION: {
                //Determinamos el archivo seleccionado
                File fichero = jFileChooser1.getSelectedFile();
                //Le asignamos un nombre
                String nombre = fichero.getName();
                //Obtenemos su extensión
                String extension = nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length());

                //Si es de extensión JPG o PNG se abrirá, si no, lanza la escepción
                if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")) {
                    try {
                        buffer = ImageIO.read(fichero);
                        repaint();
                    } catch (IOException ex) {
                        Logger.getLogger(VentanaDibujo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            break;
        }
    }//GEN-LAST:event_AbrirWindowsActionPerformed

    private void GuardarWinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarWinActionPerformed

        //Si hemos dibujado algo podremos guardar, si no salta un mensaje de error
        if (listaFormas.size() > 0) {

            //Añadimos los filtros para el tipo de archivo
            jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .JPG", ".jpg"));
            jFileChooser1.setFileFilter(new FileNameExtensionFilter("Formato .PNG", ".png"));

            //Mostramos la ventana para guardar el archivo
            int seleccion = jFileChooser1.showSaveDialog(this);

            //Mediante el switch vamos avanzando
            switch (seleccion) {
                case JFileChooser.APPROVE_OPTION: {
                    //Establecemos el fichero
                    File fichero = jFileChooser1.getSelectedFile();
                    //Le damos un nombre al fichero
                    String nombre = fichero.getName();
                    //Le añadimos la extension al fichero
                    String extension = nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length());
                    //Imprimimos por consola la extension. Solo para comprobar correcto funcionamiento
                    System.out.println(extension);

                    //Mientras la extensión sea JPG o PNG guarda el archivo
                    //Añade un mensaje que indica que el archivo se ha guardado
                    if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")) {
                        try {
                            ImageIO.write(buffer, extension, fichero);
                            JOptionPane.showMessageDialog(null, "Tu dibujo se ha guardado", "Dibujo Guardado", 1);
                        } //Si no se guarda, entramos aquí
                        catch (IOException ex) {
                            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(Level.SEVERE, null, ex);

                        }
                        //Limpiamos todo el panel
                        listaFormas.clear();
                        repaint();
                        forma = 0;
                    }
                }
                break;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Necesitas dibujar", "Error", 0);
        }
    }//GEN-LAST:event_GuardarWinActionPerformed

    //Método para centrar la aplicación
    public static void centrar(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //VentanaDibujo v = new VentanaDibujo();
                //v.setVisible(true);
                new VentanaDibujo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AbrirMac;
    private javax.swing.JMenuItem AbrirWindows;
    private javax.swing.JButton AceptarNuevoBorrar;
    private javax.swing.JButton AceptarRiesgo;
    private javax.swing.JButton BorradoBoton;
    private javax.swing.JButton BotonCirculo;
    private javax.swing.JButton BotonCruz;
    private javax.swing.JButton BotonCuadrado;
    private javax.swing.JButton BotonEstrella;
    private javax.swing.JButton BotonGoma;
    private javax.swing.JButton BotonHexagono;
    private javax.swing.JButton BotonLapiz;
    private javax.swing.JButton BotonLinea;
    private javax.swing.JButton BotonOctogono;
    private javax.swing.JButton BotonPentagono;
    private javax.swing.JButton BotonRombo;
    private javax.swing.JButton BotonTriangulo;
    private javax.swing.JButton CancelarRiesgo;
    private javax.swing.JButton Colores;
    private javax.swing.JButton DeshacerBoton;
    private javax.swing.JMenuItem DeshacerMac;
    private javax.swing.JMenuItem DeshacerWindows;
    private javax.swing.JDialog DialogoGuardarPulsadoNuevo;
    private javax.swing.JDialog DialogoLapizLento;
    private javax.swing.JDialog DialogoSelectorArchivos;
    private javax.swing.JDialog DialogoSelectorColores;
    private javax.swing.JRadioButton FilledON;
    private javax.swing.JMenuItem GuardarMac;
    private javax.swing.JButton GuardarNuevo;
    private javax.swing.JMenuItem GuardarWin;
    private javax.swing.JMenuBar MenuPaint;
    private javax.swing.JButton Nuevo;
    private javax.swing.JButton SprayBoton;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTextField valorSlider;
    // End of variables declaration//GEN-END:variables
}
